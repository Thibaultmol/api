# How to update an instance

The easiest way to update an instance while minimizing the impact on its use is to:

* stop the picture workers
* update the database schema
* update the API
* restart the updated picture workers
