# complete docker compose with geovisio backend, a database, some pictures workers and keycloak based authentication
# * All is accessible through http://localhost:8080 (using a reverse proxy to dispatch to all services)
#
# Some key variables can be changed by providing a `.env` file or environment variables to docker compose
# and feel free to change more configuration directly in this file if needed.
#
# Since this is aimed at serving an identity provider alongside goevisio, this MUST be run behind a proxy handling TLS
# and served on a domain defined in the `.env` file as DOMAIN
#
# Note: if the port 8080 is alreay binded on your system, change it in this file

x-base-geovisio: &geovisio-default
  image: geovisio/api:${GEOVISIO_IMAGE_TAG:-latest}
  build:
    context: ../..
    dockerfile: Dockerfile
    cache_from:
      - registry.gitlab.com/panoramax/server/api:build_cache

services:
  
  auth:
    build:
      context: .
      dockerfile: Dockerfile.keycloak
    command: start --optimized --import-realm
    environment:
      # Placeholders in keycloak-realm.json
      GEOVISIO_BASE_URL: https://${DOMAIN}
      GEOVISIO_CLIENT_SECRET: ${OAUTH_CLIENT_SECRET}

      # Keycloak admin account 
      KEYCLOAK_ADMIN: ${KEYCLOAK_ADMIN:-admin}
      KEYCLOAK_ADMIN_PASSWORD: ${KEYCLOAK_ADMIN_PASSWORD}

      # Database connection
      # note: keycloak uses the same database as geovisio, in a separate schema, this way there is only 1 database to backup
      KC_DB_USERNAME: keycloak_user
      KC_DB_PASSWORD: keycloak_password # you can change this password for a real world usecases, but it should not be a big deal since the database shouldn't be accessible outside the docker compose
      KC_DB_SCHEMA: keycloak
      KC_DB_URL: jdbc:postgresql://db/geovisio

      # Tell keycloak to trust nginx X-Forwarded headers
      KC_PROXY_HEADERS: xforwarded
      # It's fine to use http, the reverse proxy handle the tls termination
      KC_HTTP_ENABLED: true 
      # trust nginx to set the correct xforwared headers with the hostname
      KC_HOSTNAME_STRICT: false
      
      # tell keycloak that it will be /oauth prefix by nginx
      KC_HOSTNAME_PATH: /oauth
      # KEYCLOAK_TEMPLATES_DIR: ./themes/ # you can load theme if you want
    depends_on:
      db:
        condition: service_healthy
    networks:
      db: {}
      geovisio: {}
    healthcheck:
      # no curl in image, using cmd from https://stackoverflow.com/questions/58168955/testing-minimal-docker-containers-with-healthcheck/76790330#76790330
      test: ["CMD-SHELL", "exec 3<>/dev/tcp/127.0.0.1/8080;echo -e \"GET /oauth/health/live HTTP/1.1\r\nhost: http://localhost\r\nConnection: close\r\n\r\n\" >&3;grep \"HTTP/1.1 200 OK\" <&3"]
      timeout: 5s
      interval: 5s
      retries: 5
      start_period: 60s
    ports:
      - 8888:8080
      - 8443
    volumes:
      - ./keycloak-realm.json:/opt/keycloak/data/import/geovisio_realm.json

  migrations:
    <<: *geovisio-default
    command: db-upgrade
    environment:
      DB_URL: postgres://gvs:gvspwd@db/geovisio
    depends_on:
      db:
        condition: service_healthy
    networks:
      db: {}
      geovisio: {}

  api:
    <<: *geovisio-default
    command: ssl-api
    depends_on:
      db:
        condition: service_healthy
      migrations:
        condition: service_completed_successfully
    environment:
      OAUTH_PROVIDER: oidc
      OAUTH_CLIENT_ID: geovisio
      OAUTH_CLIENT_SECRET: ${OAUTH_CLIENT_SECRET}
      OAUTH_OIDC_URL: https://${DOMAIN}/oauth/realms/geovisio
      DB_URL: postgres://gvs:gvspwd@db/geovisio
      FLASK_SECRET_KEY: ${FLASK_SECRET_KEY}
      FLASK_SESSION_COOKIE_DOMAIN: ${DOMAIN:-localhost}
      API_FORCE_AUTH_ON_UPLOAD: True
      API_PICTURES_LICENSE_SPDX_ID: CC-BY-SA-4.0 # default pictures's license, change it if needed
      API_PICTURES_LICENSE_URL: https://spdx.org/licenses/CC-BY-SA-4.0.html
      API_BLUR_URL: https://blur.panoramax.openstreetmap.fr
      API_DERIVATES_PICTURES_PUBLIC_URL: /derivates
      API_PERMANENT_PICTURES_PUBLIC_URL: /permanent
      PICTURE_PROCESS_THREADS_LIMIT: 0
      PICTURE_PROCESS_DERIVATES_STRATEGY: PREPROCESS
      FS_URL: /data/geovisio
      NB_API_THREADS: 10
      INFRA_NB_PROXIES: ${INFRA_NB_PROXIES:-1}
    ports:
      - 5000
    volumes:
      - pic_data:/data/geovisio
    healthcheck:
      test: python -c "import requests; requests.get('http://localhost:5000/api').raise_for_status()"
      interval: 4s
      timeout: 5s
      retries: 10
      start_period: 1s
    networks:
      db: {}
      geovisio: {}

  website:
    image: geovisio/website:develop
    environment:
      VITE_INSTANCE_NAME: ${INSTANCE_NAME:-A geovisio instance}
      VITE_API_URL: /
      VITE_TILES: https://panoramax.openstreetmap.fr/pmtiles/basic.json
    ports:
      - 3000
    networks:
      geovisio: {}

  # Background workers used to process pictures in the background
  # calling the blur API and generating derivates (SD picture and tiles for faster rendering in photosphereviewer)
  # Several background workers can run together
  # Note: the blurring is configured to be done using the OSM-FR service, you can change the variable `API_BLUR_URL` if needed (in the API too).
  background-worker:
    <<: *geovisio-default
    command: picture-worker
    depends_on:
      db:
        condition: service_healthy
      migrations:
        condition: service_completed_successfully
    restart: always
    environment:
      PICTURE_PROCESS_DERIVATES_STRATEGY: PREPROCESS
      DB_URL: postgres://gvs:gvspwd@db/geovisio
      FS_URL: /data/geovisio
      API_BLUR_URL: https://blur.panoramax.openstreetmap.fr

    deploy:
      mode: replicated
      replicas: ${PICTURE_WORKERS_REPLICATS:-5} # by default this number of workers will be run. This can be change at runtime with `docker compose up background-worker -d --scale background-worker=<VALUE>`
    volumes:
      - pic_data:/data/geovisio
    networks:
      db: {}

  reverseproxy:
    image: nginx:1.25.5
    ports:
      - 8080:8080
    restart: always
    depends_on:
      api:
        condition: service_healthy
      auth:
        condition: service_healthy
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
      - pic_data:/data/geovisio
    networks:
      geovisio: {}

  db:
    environment:
      POSTGRES_DB: geovisio
      POSTGRES_PASSWORD: gvspwd
      POSTGRES_USER: gvs
    healthcheck:
      test: pg_isready -h db -q -d $$POSTGRES_DB -U $$POSTGRES_USER
      timeout: 5s
      interval: 5s
      retries: 5
      start_period: 5s
    image: postgis/postgis:16-3.4
    ports:
      - 5444:5432
    volumes:
      - postgres_data:/var/lib/postgresql/data/
      - ./1-init-keycloak-db.sh:/docker-entrypoint-initdb.d/1-init-keycloak-db.sh
    networks:
      db: {}

volumes:
  postgres_data:
    name: geovisio_auth_postgres_data

  pic_data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ${PICTURES_DIR:-./pictures_storage} # pictures are stored localy in this directory.

networks:
  db: {}
  geovisio: {}
