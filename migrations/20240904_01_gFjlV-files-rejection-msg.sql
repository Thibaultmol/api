-- files_rejection_msg
-- depends: 20240801_01_DOqmf-reports  20240813_01_T1XkO-sequences-geom-splits

ALTER TABLE files ADD COLUMN rejection_message VARCHAR;