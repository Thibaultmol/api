-- pictures_edits_triggers
-- depends: 20230420_01_elaN3-remove-picture-and-sequence-file-paths

DROP FUNCTION IF EXISTS pictures_update_sequence CASCADE;
DROP FUNCTION IF EXISTS sequences_pictures_delete CASCADE;
