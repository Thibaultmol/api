import flask
import json
from typing import Dict, Any
from flask import jsonify
from flask_babel import gettext as _, get_locale
from geovisio.web.utils import get_api_version

bp = flask.Blueprint("configuration", __name__, url_prefix="/api")


@bp.route("/configuration")
def configuration():
    """Return instance configuration informations
    ---
    tags:
        - Metadata
    responses:
        200:
            description: Information about the instance configuration
            content:
                application/json:
                    schema:
                        $ref: '#/components/schemas/GeoVisioConfiguration'
    """

    apiSum = flask.current_app.config["API_SUMMARY"]
    userLang = get_locale().language
    return jsonify(
        {
            "name": _get_translated(apiSum.name, userLang),
            "description": _get_translated(apiSum.description, userLang),
            "logo": apiSum.logo,
            "color": str(apiSum.color),
            "auth": _auth_configuration(),
            "license": _license_configuration(),
            "version": get_api_version(),
        }
    )


def _get_translated(prop: Dict[str, str], userLang) -> Dict[str, Any]:
    return {"label": prop.get(userLang, prop.get("en")), "langs": prop}


def _auth_configuration():
    from geovisio.utils import auth

    if auth.oauth_provider is None:
        return {"enabled": False}
    else:
        return {"enabled": True, "user_profile": {"url": auth.oauth_provider.user_profile_page_url()}}


def _license_configuration():
    l = {"id": flask.current_app.config["API_PICTURES_LICENSE_SPDX_ID"]}
    u = flask.current_app.config.get("API_PICTURES_LICENSE_URL")
    if u:
        l["url"] = u
    return l
